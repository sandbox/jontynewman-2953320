<?php

namespace Drupal\sql_replication_reporter\Slaves;

use Drupal\sql_replication_reporter\Slave;

/**
 * The current status of a slave in MySQL.
 */
class MysqlSlave implements Slave
{
    private $status;

    private $running;

    /**
     * Determines whether or not the given slave status can be considered as
     * running.
     *
     * @param array $status the slave status to evaluate
     * @return bool whether or not the given slave status can be considered as
     * running
     */
    public static function isRunning(array $status): bool
    {
        $running = true;

        foreach (['Slave_IO_Running', 'Slave_SQL_Running'] as $key) {

            switch(strtolower(trim($status[$key]))) {
                case 'yes':
                    // Do nothing.
                    break;
                default:
                    $running = false; // Flag the status as not running.
                    break;
            }

            // If an invalid value has been discovered, halt discovery.
            if (!$running) {
                break;
            }

        }

        return $running;
    }

    public function __construct(array $status)
    {
        $this->status = $status;
    }

    public function data()
    {
        return $this->status;
    }

    public function running(): bool
    {
        if (is_null($this->running)) {
            $this->running = self::isRunning($this->status);
        }

        return $this->running;
    }
}

