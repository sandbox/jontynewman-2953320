<?php

namespace Drupal\sql_replication_reporter;

use Drupal;
use Drupal\sql_replication_reporter\Slave;
use Drupal\sql_replication_reporter\Slaves\MysqlSlave;
use IteratorIterator;
use PDO;

/**
 * A reporter of SQL replication.
 */
class Reporter
{
    /**
     * The SQL used to retrieve slave statuses from a MySQL database management
     * system.
     */
    const SQL_MYSQL = 'SHOW SLAVE STATUS';

    /**
     * The title associated with requirements.
     */
    const TITLE = 'SQL Replication';

    /**
     * The description associated with a running slave.
     */
    const DESCRIPTION_RUNNING = 'The slave is running.';

    /**
     * The description associated with a halted slave.
     */
    const DESCRIPTION_HALTED = 'The slave is not running.';

    /**
     * The description associated with no slave statuses found.
     */
    const DESCRIPTION_NONE = 'Unable to retrieve the current slave status.';

    /**
     * Generates a list of slave statuses with the assumption that the current
     * primary database is driven by MySQL.
     *
     * @return iterable the generated list of slave statuses
     */
    public static function mysql(): iterable
    {
        $options = ['fetch' => PDO::FETCH_ASSOC];
        $statement = Drupal::database()->query(self::SQL_MYSQL, [], $options);

        return new class($statement) extends IteratorIterator
            {
                public function current()
                {
                    return new MysqlSlave(parent::current());
                }
            };
    }

    /**
     * Converts the given slave statuses to an associative array that is
     * compatible as the return value of `hook_requirements` during runtime.
     *
     * @param iterable $slaves the slave statuses to convert
     * @return array the current status as an associative array that is
     * compatible as the return value of `hook_requirements`
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Extension%21module.api.php/function/hook_requirements
     */
    public function requirements(iterable $slaves): array
    {
        $requirements = [];

        foreach ($slaves as $key => $slave) {
            $requirements[$key] = $this->toRequirement($slave);
        }

        if (!$requirements) {
            $requirements[] = [
                'title' => t(self::TITLE),
                'description' => t(self::DESCRIPTION_NONE),
                'severity' => REQUIREMENT_ERROR,
            ];
        }

        return $requirements;
    }

    /**
     * Converts the given slave status to an associative array that is
     * compatible as the return value of `hook_requirements`.
     *
     * @param \Drupal\sql_replication_reporter\Slave $slave the slave status to
     * convert
     * @return array the converted slave status
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Extension%21module.api.php/function/hook_requirements
     */
    private function toRequirement(Slave $slave): array
    {
        return [
            'title' => t(self::TITLE),
            'description' => t($this->toDescription($slave)),
            'severity' => $this->toSeverity($slave),
        ];
    }

    /**
     * Converts the given slave status to a requirement description.
     *
     * @param \Drupal\sql_replication_reporter\Slave $slave the slave status to
     * convert
     * @return string the converted slave status
     */
    private function toDescription(Slave $slave): string
    {
        return $slave->running()
            ? self::DESCRIPTION_RUNNING
            : self::DESCRIPTION_HALTED;
    }

    /**
     * Converts the given slave status to a requirement severity.
     *
     * @param \Drupal\sql_replication_reporter\Slave $slave the slave status to
     * convert
     * @return mixed the converted slave status
     */
    private function toSeverity(Slave $slave)
    {
        return $slave->running()
            ? REQUIREMENT_INFO
            : REQUIREMENT_ERROR;
    }
}

