<?php

namespace Drupal\sql_replication_reporter;

/**
 * The current status of a slave.
 */
interface Slave
{
    /**
     * Gets the data associated with the slave.
     *
     * @return mixed the data associated with the slave
     */
    public function data();

    /**
     * Determines whether or not the slave is currently running.
     *
     * @return bool whether or not the slave is currently running
     */
    public function running(): bool;
}

